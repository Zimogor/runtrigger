﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;


public enum ActionEventType { Jump, Undefined1, Undefined2 }

public class Player : MonoBehaviour {

	[SerializeField] RuneScreen runeScreen = null;

	public void OnAnimationEvent(ActionEventType eventType) {

		StartCoroutine(WaitForRune(eventType));
	}

	IEnumerator WaitForRune(ActionEventType eventType) {

		Time.timeScale = 0.0f;
		yield return runeScreen.WaitForRune(eventType);
		Time.timeScale = 1.0f;
		if (!runeScreen.LastResult)
			OnFail();
	}

	void OnFail() {

		SceneManager.LoadScene(0);
	}
}
