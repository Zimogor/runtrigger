﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;


public class Frame : MonoBehaviour, IDropHandler {

	public UnityAction<ActionEventType> OnRuneDrop;

	void IDropHandler.OnDrop(PointerEventData eventData) {

		var rune = eventData.pointerDrag.GetComponent<Rune>();
		OnRuneDrop(rune.RuneType);
	}
}
