﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;


public class Rune : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler {

	[SerializeField] ActionEventType runeType = ActionEventType.Jump;
	[SerializeField] RectTransform dragTransform = null;
	[SerializeField] Image image = null;

	RectTransform baseTransform;
	int siblindIndex;

	void Awake() {
	
		baseTransform = (RectTransform)transform.parent;
	}

	void IBeginDragHandler.OnBeginDrag(PointerEventData eventData) {

		image.raycastTarget = false;
		siblindIndex = transform.GetSiblingIndex();
		transform.SetParent(dragTransform);
	}

	void IDragHandler.OnDrag(PointerEventData eventData) {
		
		transform.position = eventData.position;
	}

	void IEndDragHandler.OnEndDrag(PointerEventData eventData) {

		image.raycastTarget = true;
		transform.SetParent(baseTransform);
		transform.SetSiblingIndex(siblindIndex);
	}

	public ActionEventType RuneType => runeType;
}
