﻿using System.Collections;
using UnityEngine;


public class RuneScreen : MonoBehaviour {

	[SerializeField] Frame frame = null;
	[SerializeField] GameObject runes = null;

	public bool LastResult { get; private set; }

	ActionEventType pendingActionType;
	bool? actionResult = null;

	void Awake() {
		
		frame.OnRuneDrop += OnFrameRuneDrop;
	}

	void OnFrameRuneDrop(ActionEventType runeType) {

		actionResult = runeType == pendingActionType;
		LastResult = actionResult.Value;
	}

	public IEnumerator WaitForRune(ActionEventType actionType) {

		actionResult = null;
		pendingActionType = actionType;
		frame.gameObject.SetActive(true);
		runes.SetActive(true);

		yield return new WaitWhile(() => actionResult == null);

		frame.gameObject.SetActive(false);
		runes.SetActive(false);
	}
}
